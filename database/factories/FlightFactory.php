<?php

use Faker\Generator as Faker;
//фабрика для заполнения БД данными
$factory->define(App\Flight::class, function (Faker $faker) {
    return [
        'flightNumber' => $faker->countryCode . $faker->randomNumber(),
        'cityFrom' => $faker->unique()->city,
        'cityTo' => $faker->unique()->city,
        'planeType' => $faker->randomElement($array = array ('EMB','RRJ','A','B')),
        'planTime'=>$faker->dateTimeBetween($startDate = 'now', $endDate = '+30 days', $timezone = null),
        'factTime'=>$faker->dateTime($max = 'now', $timezone = null),
        'status'=>$faker->randomElement($array = array ("Вылетел", "Приземлился", "Идет посадка", "Задержан"))
    ];
});
