require('./bootstrap');

window.Vue = require('vue');

import VueRouter from 'vue-router'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'


Vue.use(VueRouter)
Vue.use(BootstrapVue);

import App from './components/App'
import Home from './components/Home'
import Admin from './components/Admin'

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/admin',
            name: 'admin',
            component: Admin,
        },
    ],
});


Vue.component('flight-table', require('./components/Home.vue'))

const app = new Vue({
    el: '#app',
    components: { App },
    router,
});
