<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Flight;
class FlightController extends Controller
{
    public function index(){
        return Flight::all();
    }
    public function create(Request $request){
        $flight = new Flight();

        $flight->flightNumber = $request->flightNumber;
        $flight->cityFrom = $request->cityFrom;
        $flight->cityTo = $request->cityTo;
        $flight->planeType = $request->planeType;
        $flight->planTime = $request->planTime;
        $flight->factTime = $request->factTime;
        $flight->status = $request->status;

        $flight->save();
        return 'ok';
    }
    public function update(Request $request){
        $flight = Flight::find($request->id);

        $flight->flightNumber = $request->flightNumber;
        $flight->cityFrom = $request->cityFrom;
        $flight->cityTo = $request->cityTo;
        $flight->planeType = $request->planeType;
        $flight->planTime = $request->planTime;
        $flight->factTime = $request->factTime;
        $flight->status = $request->status;

        $flight->save();
        return 'ok';
    }

    public function delete(Request $request){
        $flight = Flight::find($request->id);

        $flight->delete();
        return 'ok';
    }

}
