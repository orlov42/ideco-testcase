Тестовое задание для ideco
=========
Исходный код SPA [демо](http://ideco-testcase.tk/)/[админка](http://ideco-testcase.tk/admin)
-------

### Используемые технологии

* Frontend - Vue, Bootstrap
* Backend - Laravel, MySQL

### Методы API

* ```(GET)    /api/flights``` - получить все рейсы
* ```(POST)   /api/flights``` - создать рейс из переданных параметров
* ```(PUT)    /api/flights``` - редактирование рейса
* ```(DELETE)  /api/flights``` - удаление рейса

#### Структура объекта
``` {id, flightNumber, cityFrom, cityTo, planeType, planTime, factTime, status} ```